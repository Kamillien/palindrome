package sheridan;

public class Palindrome {

	public static boolean isPalindrome(String s) {
		s=s.replace(" ", "").toUpperCase();
		for(int i=0,j=s.length()-1;i<j;i++,j--) {
			if(s.charAt(i)!=s.charAt(j))
			{
				return false;
			}
		}
		return true;
	}
	public static void main(String[] args) {
	System.out.println("is anna a palindrome"+Palindrome.isPalindrome("anna"));

	}

}
